const sum = require('./sum');

test('sum() should do basic additon if 2 int are provided', () => {
  const result = sum(1, 2);

  expect(result).toBe(3);
});

test('sum() should concatenate if 2 string are provided', () => {
  const result = sum('1', '2');

  expect(result).toBe('12');
});

test('sum() should return NaN if nothing is provided', () => {
  const result = sum();

  expect(result).toBeNaN();
});

test('sum() should concatenate if 2 array are provided', () => {
  const result = sum([2], [2]);

  expect(result).toBe('22');
});

test('sum() should throw and error if no value is passed into the function', () => {
  const resultFn = () => {
    sum();
  };
  //  La gestion d'erreure se passe dans la fonction, si l'erreur est pris en compte on devait enlever le .not
  expect(resultFn).not.toThrow();
});
